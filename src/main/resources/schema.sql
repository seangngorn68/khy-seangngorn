
CREATE TABLE product
 (
     product_id  serial PRIMARY KEY,
     product_name  varchar(200)     NOT NULL,
     product_price double precision NOT NULL
);
CREATE TABLE customer
(
    customer_id      serial PRIMARY KEY,
    customer_name    varchar(200) NOT NULL,
    customer_address varchar(500) NOT NULL,
    customer_phone   varchar(10)  NOT NULL
);

CREATE TABLE invoice
(
    invoice_id   serial PRIMARY KEY,
    invoice_date timestamp DEFAULT NOW() NOT NULL,
    customer_id  integer  NOT NULL REFERENCES public.customer ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE invoice_detail
(
    id serial PRIMARY KEY,
    invoice_id integer NOT NULL REFERENCES public.invoice ON DELETE CASCADE,
    product_id integer NOT NULL  REFERENCES public.product  ON UPDATE CASCADE ON DELETE CASCADE
);

