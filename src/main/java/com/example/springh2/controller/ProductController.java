package com.example.springh2.controller;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.repsponse.ApiResponse;
import com.example.springh2.model.entity.Product;
import com.example.springh2.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products/")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    public List<Product> getAllProduct(){
        return  productService.getAllProduct();
    }
    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer productId ){
        Product product =productService.getProductById(productId);
        ApiResponse<Product> response= new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully get data",
                product


        );
        return ResponseEntity.ok(response);
    }
    @PostMapping("/add-new-customer")
    public ResponseEntity<?>insertProduct(@RequestBody Product product){
        return ResponseEntity.ok(
                new ApiResponse<Product>(
                        LocalDateTime.now(),
                        HttpStatus.OK,
                        "Successfully insert data",
                        productService.insertProduct(product)
                )
        );
    }
    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer productId, @RequestBody ProductRequest productRequest) {
        productService.updateProductById(productId, productRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully update data",
                null
        ));
    }
    @DeleteMapping("delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer productId){
        productService.deleteProductById(productId);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully Delete data",
                null
        ));
    }
}

