package com.example.springh2.service;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer id);

    Product insertProduct(Product product);


    void updateProductById(Integer productId, ProductRequest productRequest);

    void deleteProductById(Integer productId);
}
