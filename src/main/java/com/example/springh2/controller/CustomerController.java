package com.example.springh2.controller;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.entity.Customer;
import com.example.springh2.model.entity.Product;
import com.example.springh2.model.repsponse.ApiResponse;
import com.example.springh2.model.request.CustomerRequest;
import com.example.springh2.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers/")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("/get-all-customer")
    public List<Customer> getAllCustomer(){
     return  customerService.getAllCustomer();
    }
    @GetMapping("get-customer-by-id/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer customer_id){
        Customer customer= customerService.getCustomerById(customer_id);
        ApiResponse<Customer> customerApiResponse =new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully to get data",
                customer
        );
          return ResponseEntity.ok(customerApiResponse);
    }
    @PostMapping("/add-new-customer")
    public ResponseEntity<?>insertCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(
                new ApiResponse<Customer>(
                        LocalDateTime.now(),
                        HttpStatus.OK,
                        "Successfully insert data",
                        customerService.insertCustomer(customer)
                )
        );
    }
    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer customerId, @RequestBody CustomerRequest customerRequest) {
        customerService.updateCustomerById(customerId, customerRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully update data",
                null
        ));
    }
}
