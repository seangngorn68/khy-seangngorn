package com.example.springh2.repository;

import com.example.springh2.model.entity.Customer;
import com.example.springh2.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("""
            SELECT * FROM  customer_tb
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    List<Customer> getAllCustomer();
    @Select("""
            SELECT * FROM  customer_tb WHERE  customer_id=#{customerId};
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer getCustomerById(Integer customerId);



    @Update("""
            UPDATE customer_tb SET customer_name=#{customer.customerName}, customer_address=#{customer.customerAddress}, customer_phone=#{customer.customerPhone}
             WHERE  customer_id = #{customerId};
            """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    void updateCustomer(Integer customerId, @Param("customer") CustomerRequest customerRequest);
    @Select("""
             INSERT INTO customer_tb (customer_name,customer_address,customer_phone) VALUES (#{customer.customerName},#{customer.customerAddress},#{customer.customerPhone});
            RETURNING*
             """)
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    Customer insertCustomer(Customer customer);
    @Result(property = "customerId",column = "customer_id")
    @Result(property = "customerName",column = "customer_name")
    @Result(property = "customerAddress",column = "customer_address")
    @Result(property = "customerPhone",column = "customer_phone")
    @Delete("""
            DELETE customer_tb WHERE customer_id = #{CustomerId};
            """)
    void deleteCustomerById(Integer customerId);
}
