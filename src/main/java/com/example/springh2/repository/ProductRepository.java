package com.example.springh2.repository;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface ProductRepository {
    @Select("""
            SELECT * FROM product_tb
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    List<Product> getAllProduct();
    @Select("""
            SELECT * FROM product_tb WHERE product_id=#{productId};
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    Product getProductById(Integer productId);
    @Select("""
             INSERT INTO product_tb (product_name,product_price) VALUES (#{product.productName},#{product.productPrice});
            RETURNING*
             """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    Product insertProduct(@Param("product") Product product);
    @Update("""
            UPDATE product_tb SET product_name=#{product.productName},product_price=#{product.productPrice}
             WHERE  product_id = #{productId};
            """)
    @Result(property = "productId",column = "product_id")
    @Result(property = "productName",column = "product_name")
    @Result(property = "productPrice",column = "product_price")
    void updateProduct(Integer productId ,@Param("product") ProductRequest productRequest);
    @Delete("""
            DELETE product_tb WHERE product_id = #{productId};
            """)
    void deleteProductById(Integer productId);
}
