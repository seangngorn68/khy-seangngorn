package com.example.springh2.service.serviceImp;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.entity.Customer;
import com.example.springh2.model.entity.Product;
import com.example.springh2.model.request.CustomerRequest;
import com.example.springh2.repository.CustomerRepository;
import com.example.springh2.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;
    @Autowired
    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insertCustomer(customer);
    }

    @Override
    public void updateCustomerById(Integer customerId, CustomerRequest customerRequest) {
        customerRepository.updateCustomer(customerId,customerRequest);

    }
    @Override
    public void deleteCustomerById(Integer customerId) {
        customerRepository.deleteCustomerById(customerId);
    }
}
