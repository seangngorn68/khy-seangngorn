package com.example.springh2.service;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.entity.Customer;
import com.example.springh2.model.entity.Product;
import com.example.springh2.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer customerId);
   Customer insertCustomer(Customer customer);
    void updateCustomerById(Integer customerId, CustomerRequest customerRequest) ;
    void deleteCustomerById(Integer CustomerId);

}
