package com.example.springh2.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer productId;
    private String productName;
    private  Double productPrice;

}
