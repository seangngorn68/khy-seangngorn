package com.example.springh2.service.serviceImp;

import com.example.springh2.model.ProductRequest;
import com.example.springh2.model.entity.Product;
import com.example.springh2.repository.ProductRepository;
import com.example.springh2.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

    @Service
    public class ProductImp implements ProductService {
        @Autowired
        public void setProductRepository(ProductRepository productRepository) {
            this.productRepository = productRepository;
        }
        private ProductRepository productRepository;
        @Override
        public List<Product> getAllProduct() {
            return productRepository.getAllProduct();
        }

        @Override
        public Product getProductById(Integer productId) {
            return productRepository.getProductById(productId);
        }

        @Override
        public Product insertProduct(Product product) {
            return productRepository.insertProduct(product);
        }

        @Override
        public void updateProductById(Integer productId, ProductRequest productRequest) {
            productRepository.updateProduct(productId,productRequest);
        }

        @Override
        public void deleteProductById(Integer productId) {
            productRepository.deleteProductById(productId);
        }

    }
